package com.match.management.domain.match;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Team {
    List<Player> players;
    CallCount callCount;

    public void reportMissing() {
        if(callCount.isFirstCall()){
            this. callCount = callCount.secondCall();
            return;
        }
        if(callCount.isSecondCall()){
            this.callCount = callCount.thirdCall();
        }
    }

    public boolean containsPlayers(List<PlayerId> playerIds) {
        return players.stream().map(Player::getId).anyMatch(playerIds::contains);
    }

}
