package com.match.management.domain.match;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CallCount {
    private final int value;
    private final LocalDateTime timeOfLastCall;

    public static CallCount firstCall() {
        return new CallCount(1, LocalDateTime.now());
    }

    public CallCount secondCall() {
        return new CallCount(2, LocalDateTime.now());
    }

    public CallCount thirdCall() {
        return new CallCount(3, LocalDateTime.now());
    }

    public boolean isFirstCall() {
        return value == 1;
    }

    public boolean isSecondCall() {
        return value == 2;
    }
}
