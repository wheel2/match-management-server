package com.match.management.domain.match;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;


@Data
@AllArgsConstructor
public class Player {
    @NonNull
    private final PlayerId id;
    private final String firstName;
    private final String lastName;
    private final Club club;

}
