package com.match.management.infrastructure.web;

import com.match.management.domain.CallForMissingPlayerRequestedEvent;
import com.match.management.domain.ResultUpdatedEvent;
import com.match.management.domain.TTTEvent;
import com.match.management.domain.TableUpdatedEvent;
import com.match.management.domain.match.MatchId;
import com.match.management.domain.match.MatchRepository;
import com.match.management.domain.table.TableId;
import com.match.management.domain.table.TableRepository;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;

import javax.annotation.PostConstruct;

import static reactor.bus.selector.Selectors.type;

@Service
public class TableWebSocketController implements Consumer<Event<TTTEvent>> {

    private final SimpMessagingTemplate template;
    private final TableRepository tableRepository;
    private final MatchRepository matchRepository;
    private final EventBus eventBus;

    public TableWebSocketController(SimpMessagingTemplate template, TableRepository tableRepository,
                                    MatchRepository matchRepository, EventBus eventBus) {
        this.template = template;
        this.tableRepository = tableRepository;
        this.matchRepository = matchRepository;
        this.eventBus = eventBus;
    }

    @PostConstruct
    public void init() {
        eventBus.on(type(TTTEvent.class), this);
    }

    private void tableUpdate(MatchId matchId) {
        tableUpdate(tableRepository.findTable(matchId).getId());
    }

    private void tableUpdate(TableId tableId) {
        template.convertAndSend(
                "/topic/table",
                TableDTO.from(tableRepository.findTable(tableId), matchRepository::findById)
        );
    }

    @Override
    public void accept(Event<TTTEvent> event) {
        if (event.getData() instanceof ResultUpdatedEvent) {
            tableUpdate(((ResultUpdatedEvent) event.getData()).getMatchId());
        } else if (event.getData() instanceof TableUpdatedEvent) {
            tableUpdate(((TableUpdatedEvent) event.getData()).getTableId());
        } else if (event.getData() instanceof CallForMissingPlayerRequestedEvent) {
            tableUpdate(((CallForMissingPlayerRequestedEvent) event.getData()).getTableId());
        }
    }

}
