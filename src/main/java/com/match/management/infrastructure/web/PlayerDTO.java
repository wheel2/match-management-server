package com.match.management.infrastructure.web;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.match.management.domain.match.*;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonNaming(SnakeCaseStrategy.class)
public class PlayerDTO {
    private long playerId;
    private String club;
    private String firstName;
    private String lastName;
    private int callCount;

    public static PlayerDTO from(Player player, CallCount callCount) {
        return PlayerDTO.builder()
                .club(player.getClub().getName())
                .firstName(player.getFirstName())
                .lastName(player.getLastName())
                .playerId(player.getId().getValue())
                .callCount(callCount.getValue())
                .build();
    }

    public static Player to(PlayerDTO player) {
        if (player == null) {
            return null;
        }
        return new Player(new PlayerId(player.getPlayerId()), player.getFirstName(), player.getLastName(), new Club(player.getClub()));
    }

    public static List<PlayerDTO> from(Team team) {
        return team.getPlayers().stream().map(player -> PlayerDTO.from(player, team.getCallCount())).collect(Collectors.toList());
    }

    public static Team to(List<PlayerDTO> players) {
        return new Team(players.stream()
                .map(PlayerDTO::to)
                .collect(Collectors.toList()), CallCount.firstCall());
    }
}
