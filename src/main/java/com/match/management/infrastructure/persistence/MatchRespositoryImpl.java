package com.match.management.infrastructure.persistence;

import com.match.management.domain.match.Match;
import com.match.management.domain.match.MatchId;
import com.match.management.domain.match.MatchRepository;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class MatchRespositoryImpl implements MatchRepository {

    private static ConcurrentHashMap<MatchId, Match> matches = new ConcurrentHashMap<>();

    @Override
    public Match findById(MatchId matchId) {
        return matches.get(matchId);
    }

    @Override
    public void save(Match match) {
        matches.put(match.getId(), match);
    }

    @Override
    public void remove(MatchId matchId) {
        matches.remove(matchId);
    }

}
