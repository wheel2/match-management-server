package com.match.management;

import com.match.management.application.MatchService;
import com.match.management.domain.match.GameResult;
import com.match.management.domain.match.MatchId;
import com.match.management.domain.match.Result;
import com.match.management.domain.table.Table;
import com.match.management.domain.table.TableId;
import com.match.management.domain.table.TableRepository;
import com.match.management.infrastructure.web.*;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class TTTApplicationRunner {

    private final MockingResource mockingResource;

    private final MatchService matchService;

    private final TableRepository tableRepository;

    public TTTApplicationRunner(MockingResource mockingResource, MatchService matchService, TableRepository tableRepository) {
        this.mockingResource = mockingResource;
        this.matchService = matchService;
        this.tableRepository = tableRepository;
    }

    @EventListener
    public void initRepositoriesWithMockData(ApplicationStartedEvent event) {
        for (int i = 1; i < 26; i++) {
            mockingResource.postEvent(ExternalEventDTO.builder()
                    .id(ExternalEventId.MATCHES_ASSIGNED_TO_TABLE)
                    .payload(EventPayloadDTO.builder()
                            .tableId(String.valueOf(i))
                            .matches(Arrays.asList(
                                    MatchDTO.builder()
                                            .matchId(i * 10)
                                            .classification("Herren A")
                                            .stage("Gruppe " + i)
                                            .playersA(createDouble(i, i * 100, "Peter", "Lustig", "TSV Basel"))
                                            .playersB(createDouble(i, i * 100 + 1, "Willi", "Meier", "TSV Zürich"))
                                            .result(ResultDTO.builder()
                                                    .games(Collections.emptyList())
                                                    .build())
                                            .build(),
                                    MatchDTO.builder()
                                            .matchId(i * 10 + 5)
                                            .classification("Herren A")
                                            .stage("Gruppe " + i)
                                            .playersA(createSingle(i, i * 100 + 50, "Steve", "Jobs", "TSV Cupertino"))
                                            .playersB(createSingle(i, i * 100 + 51, "Bill", "Gates", "TSV Richmond"))
                                            .result(ResultDTO.builder()
                                                    .games(Collections.emptyList())
                                                    .build())
                                            .build()
                                    )
                            )
                            .build())
                    .build());

        }
        tableRepository.save(Table.builder().id(new TableId("15")).build());

        matchService.updateResult(
                new MatchId(10),
                new Result(Arrays.asList(
                        new GameResult(8, 11),
                        new GameResult(12, 10),
                        new GameResult(29, 27),
                        new GameResult(11, 0)
                ))
        );
        matchService.finish(new MatchId(10));

    }

    private List<PlayerDTO> createDouble(int index, int playerid, String firstName, String lastName, String clubName) {
        return Arrays.asList(
                createSinglePlayer(index, playerid, firstName, lastName, clubName),
                createSinglePlayer(index, playerid, firstName, lastName, clubName)
        );
    }

    private List<PlayerDTO> createSingle(int i, int playerId, String firstName, String lastName, String clubName) {
        return Arrays.asList(
                createSinglePlayer(i, playerId, firstName, lastName, clubName)
        );
    }

    private PlayerDTO createSinglePlayer(int index, int playerId, String firstName, String lastName, String clubName) {
        return PlayerDTO.builder()
                .playerId(playerId)
                .firstName(firstName)
                .lastName(lastName + index)
                .club(clubName)
                .build();
    }

}
