FROM maven:3.5.4-jdk-8-alpine AS build  
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app  
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8-jre-alpine
RUN mkdir /opt/app/
COPY --from=build /usr/src/app/target/*.jar /opt/app/app.jar
ADD entrypoint.sh /opt/app/entrypoint.sh
EXPOSE 8080
ENTRYPOINT ["sh", "/opt/app/entrypoint.sh"]
